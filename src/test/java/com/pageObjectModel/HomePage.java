package com.pageObjectModel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {
	
public WebDriver driver;	
@FindBy(id="hp-widget__sfrom")
public WebElement flightsFrom;

@FindBy(id="hp-widget__sTo")
public WebElement flightsTo;

@FindBy(id="searchBtn")
public WebElement flightSearchButton;

public HomePage(WebDriver driver){
	this.driver=driver;
}

public void seachFlights(String strFrom,String strTo){
	flightsFrom.clear();
	flightsFrom.sendKeys(strFrom);
	flightsTo.clear();
	flightsTo.sendKeys(strTo);
	flightSearchButton.click();	
}
public String getTitle(){
	String title = driver.getTitle();
	return title;
}

}
