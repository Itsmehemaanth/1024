package com.testing;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import reusableComponents.Excel;

import com.pageObjectModel.HomePage;

public class TC001 {
	HomePage homePage ;
	public WebDriver driver;
	public Excel excel;
	
	
  @BeforeTest
  public void setup() throws IOException{
	  excel =new Excel(System.getProperty("user.dir")+"\\src\\test\\java\\testData\\TestScripts.xlsx","TestData","TC001");
	  System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\src\\test\\resources\\chromedriver.exe");
      driver = new ChromeDriver();              
      //driver.get("https://www.makemytrip.com/");
      driver.get(excel.getData("URL"));
      homePage = PageFactory.initElements(driver,HomePage.class); 
  }
	
  @Test
  public void TC1() throws IOException {	 
	  System.out.println(homePage.getTitle());
	  
	  if(homePage.getTitle().contains(excel.getData("Assertion_Text1"))){
		  Assert.assertTrue(true,"Navigated to homePage");		  
	  }else{
		  Assert.assertTrue(false,"Issue Navigating to homePage");
	  }
      homePage.seachFlights(excel.getData("FlightFrom"),excel.getData("FlightTo"));

	  Assert.assertEquals(homePage.getTitle(),excel.getData("Assertion_Text2"));
  }

  @AfterTest
  public void tearDown(){
	  driver.quit();
  }
  
}
